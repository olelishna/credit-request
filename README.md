# HW28. Проектирование API

Необходимо реализовать Rest API с использованием очередей.
Ваши клиенты будут отправлять запросы на обработку, а вы будете складывать их в очередь и возвращать номер запроса.
В фоновом режиме вы будете обрабатывать запросы, а ваши клиенты периодически, используя номер запроса, будут проверять
статус его обработки.

Разрешается

* Использование Composer-зависимостей
* Использование микрофреймворков (Lumen, Silex и т.п.)

Критерии оценки

* 5 баллов за реализацию API
* 3 балла за применение очередей
* 2 балла за документацию (например, в Swagger)

# HW27. Работа с очередью

Мы научимся строить асинхронное приложение на практике.
Задача состоит в том, чтобы научиться применять очередь, увидеть её преимущества и недостатки.

**Описание/Пошаговая инструкция выполнения домашнего задания:**

Пишем приложение обработки отложенных запросов.

1. Создать простое веб-приложение, принимающее POST запрос из формы от пользователя. Например, запрос на генерацию
   банковской выписки за указанные даты.
    1. Обычно такие запросы (в реальных системах) работают довольно долго, поэтому пользователя надо оповестить о том,
       что запрос принят в обработку
    2. Форма должна подразумевать отправку оповещения по результатам работы
2. Передать тело запроса в очередь
3. Написать скрипт, который будет читать сообщения из очереди и выводить информацию о них в консоль
4. Реализация оповещения
5. Сгенерированный ответ отправить через email или telegram
6. Приложить инструкцию по запуску системы

**Критерии оценки:**

1. Работоспособность решения (5 баллов)
2. Чистота кода (3 балла)
3. Инструкции по развёртыванию системы (2 балла)

# HW20. Паттерны проектирования

Разработать небольшое приложение для работы с заявками на кредит, которое будет состоять из
3 независимых модулей:

## Модуль 1. Валидация формы

У вас есть форма, которая структурирована вот так:

* ФИО (составное поле)
    * фамилия (простое поле)
    * имя (простое поле)
    * отчество (простое поле)
* Паспорт (составное поле)
    * серия и номер (простое поле)
    * кем выдан (составное поле)
        * код подразделения (простое поле)
        * дата выдачи (простое поле)

Данные из этой формы приходят в виде вложенного массива.
Вам нужно выполнить валидацию этого массива, разработав:

* 4 валидатора (имя физлица, серия и номер паспорта, код подразделения, дата выдачи паспорта) — **Стратегия**
* 2 поля формы (простое или составное; в простое поле “встраивается” тот или иной валидатор) — **Компоновщик**
* фабрику для простых/составных полей формы — **Фабричный метод**

Вам также потребуется пример клиентского кода, в котором вы:

* создадите нужное кол-во полей формы (с соответствующими валидаторами)
* соберёте из них дерево
* запустите валидацию в корне этого дерева
* после успешной валидации отправляет сообщение через Диспетчер событий всем подписчикам (см. ниже)

## Модуль 2. Диспетчер событий

Вам нужно реализовать **Наблюдатель**, взяв за основу стандарт PSR-14 (https://www.php-fig.org/psr/psr-14/). Его
подписчиком
будет Шлюз (см. ниже).

## Модуль 3. Шлюз

Этот модуль работает с сущностью Заявка, которая физически хранится на другом микросервисе. Для работы с ней вам
потребуется:

* класс Заявки (2-3 любых поля, это не принципиально)
* **Прокси**, который будет “притворяться” Заявкой и при каждом изменении полей “отправлять” запросы во внешний
  миросервис (имитация http-запросов)
* дополнительный класс (сервис), который будет создавать эти Заявки

В итоге получится примерно такая схема:

* Модуль 1 выполняет валидацию и публикует событие в Модуль 2
* Модуль 2 перенаправляет событие подписчику в Модуле 3 (сервису)
* Модуль 3 берёт событие и создаёт на его основе Заявку

## Application

![Application](docs/images/app.png)